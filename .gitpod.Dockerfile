FROM gitpod/workspace-full

# Install custom tools, runtimes, etc.
# For example "bastet", a command-line tetris clone:
# RUN brew install bastet
#
# More information: https://www.gitpod.io/docs/config-docker/

RUN apt-get update \
 && apt-get install -y \
  apt-utils \
  sudo \
  git \
  less \
  wget

RUN mkdir /home/gitpod/.conda
# Install conda
RUN wget --quiet https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda.sh && \
    sudo /bin/bash ~/miniconda.sh -b -p /opt/conda && \
    sudo ln -s /opt/conda/etc/profile.d/conda.sh /etc/profile.d/conda.sh && \
    sudo echo ". /opt/conda/etc/profile.d/conda.sh" >> ~/.bashrc && \
    sudo echo "conda activate base" >> ~/.bashrc
#     rm ~/miniconda.sh && \

RUN sudo chown -R gitpod:gitpod /opt/conda \
    && sudo chmod -R 777 /opt/conda \
    && sudo chown -R gitpod:gitpod /home/gitpod/.conda \
    && sudo chmod -R 777 /home/gitpod/.conda

RUN sudo chown -R gitpod:gitpod /home/gitpod/.cache \
    && sudo chmod -R 777 /home/gitpod/.cache

#Install Python Packages
 COPY requirements.txt /tmp/
 #RUN  pip3 install --requirement /tmp/requirements.txt
 RUN cat /tmp/requirements.txt | sed -e '/^\s*#.*$/d' -e '/^\s*$/d' | xargs -n 1 pip3 install

 RUN apt-get clean