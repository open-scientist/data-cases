Copy-pasting in console

- gcp + VM (€/second) for now (comme VPS mais plus grand et moins cher ?? )
- puis doker
- puis helm-chart (k8s)
- puis Flynn (partagé ?)

- diagrammes : draw.io

- **gitpod** (avec extension déjà installée : python & ipynb) (pas de copier-coller-terminal & pas de ctrl+z, Edit>Undo)
- commencez avec l'instance partagée et tout de suite, allez sur votre branche
- pour avoir le droit de retourner sur master : il faut que vous avez pushé sur votre branche
- et quand j'avance sur master, il faut que vous vous mettiez à jour. Régulièrement, sinon ça va vous embrouiller

- on commence par hacker sur jupyter
- et on modularise, les notebooks ne sont alors plus que des portes d'entrée

- on versionne déjà le code, versionnons les les données
- on fait gaffe au **tree directory** tree avec **cookie cutter** // convention  https://drivendata.github.io/cookiecutter-data-science/#analysis-is-a-dag
- et on gère les données dans deux répos séparés : mais ne pas les gérer comme des submodules, les gérer avec dvc https://gitlab.com/open-scientist/data-cases-data data : celui-ci est public
- cd data ; dvc get https://gitlab.com/open-scientist/data-cases-data-confidential  : celui-là est privé
- gitlab n'est pas le meilleur endroit pour stocker de gros jeux de données, voici des notebooks pour vous guider à utiliser ssh et drive
- on ignore ces dossiers de données depuis l'environnement de développement. On les gère depuis un autre environnement


- allez go, un premier cas dvc get
- dvc get https://github.com/iterative/dataset-registry data.xml
- dvc add data/data.xml
- git add data/data.xml.dvc data/.gitignore
- git commit -m "Add raw data"

- allez, calculons tel calcul qui va être super lourd computationnellement -> go le faire sur gcp // une machine virtuelle là où vous pouvez