# Data cases

Installez l'extension Gitpod à votre navigateur [Firefox](https://addons.mozilla.org/en-US/firefox/addon/gitpod/)   [Chrome](https://chrome.google.com/webstore/detail/gitpod-dev-environments-i/dodmmooeoklaejobgleioelladacbeki)

## Session 1

Changez de session avec les boutons ci-dessous

Notebook du jour, accédez-y et cliquez sur le bouton de l'extension Gitpod.

Option 2 : accéder à l'espace partagé.

Extrait à venir. [Accéder à la page complète]()

---

## Menu

[Appliquez à vos données](Fork)

[Projet pédagogique (features)](https://gitlab.com/open-scientist/hycar/-/blob/master/README.md#features)

[Séances (cases)(architecture)](https://gitlab.com/open-scientist/hycar/-/blob/master/README.md#architecture)

[Historique de développement du cours](https://gitlab.com/open-scientist/hycar/-/blob/master/README.md#historique-du-développement)

[Comment contribuer](https://gitlab.com/open-scientist/hycar/-/blob/master/README.md#comment-contribuer)

----

[Session 1]() [Session 2]() [Session 3]() [Session 4]() [Session 5]() [Session 6]() 