[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/open-scientist/data-cases)

Temporairement, exécutez les commandes d'installation de conda présentes dans .gitpod.yml

# Data cases

## Rationale

Extrait à venir. [Accéder à la page complète]()

## Accéder au cours

[open-scientist.gitlab.io/hycar](https://open-scientist.gitlab.io/hycar)

## Features

= Projet pédagogique

Extrait à venir. [Accéder à la page complète]()

## Architecture

= cases

Extrait à venir. [Accéder à la page complète]()

## Historique du développement

Parcours data SHS 1, 2

Extrait à venir. [Accéder à la page complète]()

## Comment contribuer

Extrait à venir. [Accéder à la page complète]()
